=========
Scheduler
=========

.. module:: slixmpp.xmlstream.scheduler

.. autoclass:: Task
    :members:

.. autoclass:: Scheduler
    :members:
