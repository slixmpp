Jabber IDs (JID)
=================

.. module:: slixmpp.xmlstream.jid

.. autoclass:: JID
    :members:
