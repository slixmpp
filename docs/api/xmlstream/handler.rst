Stanza Handlers
===============

The Basic Handler
-----------------
.. module:: slixmpp.xmlstream.handler.base

.. autoclass:: BaseHandler
    :members:

Callback
--------
.. module:: slixmpp.xmlstream.handler.callback

.. autoclass:: Callback
    :members:


Waiter
------
.. module:: slixmpp.xmlstream.handler.waiter

.. autoclass:: Waiter
    :members:
