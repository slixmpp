import logging

logging.warning('Deprecated: slixmpp.xmlstream.jid is moving to slixmpp.jid')

from slixmpp.jid import JID
